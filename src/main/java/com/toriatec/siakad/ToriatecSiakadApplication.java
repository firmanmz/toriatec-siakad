package com.toriatec.siakad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToriatecSiakadApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToriatecSiakadApplication.class, args);
	}
}
