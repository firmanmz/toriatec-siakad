package com.toriatec.siakad.admin.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the siswa database table.
 * 
 */
@Entity
@NamedQuery(name="Siswa.findAll", query="SELECT s FROM Siswa s")
public class Siswa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="alamat_sekolah_dasar")
	private String alamatSekolahDasar;

	@Column(name="alamat_sekolah_menengah_atas")
	private String alamatSekolahMenengahAtas;

	@Column(name="alamat_sekolah_menengah_pertama")
	private String alamatSekolahMenengahPertama;

	@Column(name="alamat_sekolah_tk")
	private String alamatSekolahTk;

	@Column(name="alamat_tinggal")
	private String alamatTinggal;

	@Column(name="asal_sekolah_dasar")
	private String asalSekolahDasar;

	@Column(name="asal_sekolah_menengah_atas")
	private String asalSekolahMenengahAtas;

	@Column(name="asal_sekolah_menengah_pertama")
	private String asalSekolahMenengahPertama;

	@Column(name="asal_sekolah_tk")
	private String asalSekolahTk;

	@Column(name="berat_badan_kg")
	private float beratBadanKg;

	@Column(name="created_by")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="file_akte_kelahiran")
	private String fileAkteKelahiran;

	@Column(name="file_izasah_terakhir")
	private String fileIzasahTerakhir;

	@Column(name="file_kartu_keluarga")
	private String fileKartuKeluarga;

	@Column(name="golongan_darah")
	private String golonganDarah;

	@Column(name="jarak_rumah_sekolah_meter")
	private float jarakRumahSekolahMeter;

	@Column(name="jenis_kelamin")
	private String jenisKelamin;

	@Column(name="jumlah_saudara_kandung")
	private int jumlahSaudaraKandung;

	private String kegemaran;

	@Column(name="keterangan_status")
	private String keteranganStatus;

	@Column(name="modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="nama_ayah_kandung")
	private String namaAyahKandung;

	@Column(name="nama_ibu_kandung")
	private String namaIbuKandung;

	@Column(name="nama_lengkap")
	private String namaLengkap;

	@Column(name="nama_panggilan")
	private String namaPanggilan;

	@Column(name="nama_wali")
	private String namaWali;

	private String nik;

	private String nisn;

	@Column(name="no_hp")
	private String noHp;

	@Column(name="no_telp")
	private String noTelp;

	@Column(name="pekerjaan_ayah")
	private String pekerjaanAyah;

	@Column(name="pekerjaan_ibu")
	private String pekerjaanIbu;

	@Column(name="pekerjaan_wali")
	private String pekerjaanWali;

	@Column(name="pendidikan_ayah")
	private String pendidikanAyah;

	@Column(name="pendidikan_ibu")
	private String pendidikanIbu;

	@Column(name="pendidikan_wali")
	private String pendidikanWali;

	@Column(name="penghasilan_ayah_bulanan")
	private int penghasilanAyahBulanan;

	@Column(name="penghasilan_ibu_bulanan")
	private int penghasilanIbuBulanan;

	@Column(name="penghasilan_wali_bulanan")
	private int penghasilanWaliBulanan;

	@Column(name="prestasi_id")
	private int prestasiId;

	@Column(name="status_profil")
	private String statusProfil;

	@Temporal(TemporalType.DATE)
	@Column(name="tanggal_lahir")
	private Date tanggalLahir;

	@Temporal(TemporalType.DATE)
	@Column(name="tanggal_lahir_ayah")
	private Date tanggalLahirAyah;

	@Temporal(TemporalType.DATE)
	@Column(name="tanggal_lahir_ibu")
	private Date tanggalLahirIbu;

	@Temporal(TemporalType.DATE)
	@Column(name="tanggal_lahir_wali")
	private Date tanggalLahirWali;

	@Column(name="tempat_lahir")
	private String tempatLahir;

	@Column(name="tempat_lahir_ayah")
	private String tempatLahirAyah;

	@Column(name="tempat_lahir_ibu")
	private String tempatLahirIbu;

	@Column(name="tempat_lahir_wali")
	private String tempatLahirWali;

	@Column(name="tinggal_bersama")
	private String tinggalBersama;

	@Column(name="tinggi_badan_cm")
	private float tinggiBadanCm;

	//bi-directional many-to-one association to Sekolah
	@ManyToOne
	@JoinColumn(name="id_sekolah")
	private Sekolah sekolah;

	public Siswa() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAlamatSekolahDasar() {
		return this.alamatSekolahDasar;
	}

	public void setAlamatSekolahDasar(String alamatSekolahDasar) {
		this.alamatSekolahDasar = alamatSekolahDasar;
	}

	public String getAlamatSekolahMenengahAtas() {
		return this.alamatSekolahMenengahAtas;
	}

	public void setAlamatSekolahMenengahAtas(String alamatSekolahMenengahAtas) {
		this.alamatSekolahMenengahAtas = alamatSekolahMenengahAtas;
	}

	public String getAlamatSekolahMenengahPertama() {
		return this.alamatSekolahMenengahPertama;
	}

	public void setAlamatSekolahMenengahPertama(String alamatSekolahMenengahPertama) {
		this.alamatSekolahMenengahPertama = alamatSekolahMenengahPertama;
	}

	public String getAlamatSekolahTk() {
		return this.alamatSekolahTk;
	}

	public void setAlamatSekolahTk(String alamatSekolahTk) {
		this.alamatSekolahTk = alamatSekolahTk;
	}

	public String getAlamatTinggal() {
		return this.alamatTinggal;
	}

	public void setAlamatTinggal(String alamatTinggal) {
		this.alamatTinggal = alamatTinggal;
	}

	public String getAsalSekolahDasar() {
		return this.asalSekolahDasar;
	}

	public void setAsalSekolahDasar(String asalSekolahDasar) {
		this.asalSekolahDasar = asalSekolahDasar;
	}

	public String getAsalSekolahMenengahAtas() {
		return this.asalSekolahMenengahAtas;
	}

	public void setAsalSekolahMenengahAtas(String asalSekolahMenengahAtas) {
		this.asalSekolahMenengahAtas = asalSekolahMenengahAtas;
	}

	public String getAsalSekolahMenengahPertama() {
		return this.asalSekolahMenengahPertama;
	}

	public void setAsalSekolahMenengahPertama(String asalSekolahMenengahPertama) {
		this.asalSekolahMenengahPertama = asalSekolahMenengahPertama;
	}

	public String getAsalSekolahTk() {
		return this.asalSekolahTk;
	}

	public void setAsalSekolahTk(String asalSekolahTk) {
		this.asalSekolahTk = asalSekolahTk;
	}

	public float getBeratBadanKg() {
		return this.beratBadanKg;
	}

	public void setBeratBadanKg(float beratBadanKg) {
		this.beratBadanKg = beratBadanKg;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFileAkteKelahiran() {
		return this.fileAkteKelahiran;
	}

	public void setFileAkteKelahiran(String fileAkteKelahiran) {
		this.fileAkteKelahiran = fileAkteKelahiran;
	}

	public String getFileIzasahTerakhir() {
		return this.fileIzasahTerakhir;
	}

	public void setFileIzasahTerakhir(String fileIzasahTerakhir) {
		this.fileIzasahTerakhir = fileIzasahTerakhir;
	}

	public String getFileKartuKeluarga() {
		return this.fileKartuKeluarga;
	}

	public void setFileKartuKeluarga(String fileKartuKeluarga) {
		this.fileKartuKeluarga = fileKartuKeluarga;
	}

	public String getGolonganDarah() {
		return this.golonganDarah;
	}

	public void setGolonganDarah(String golonganDarah) {
		this.golonganDarah = golonganDarah;
	}

	public float getJarakRumahSekolahMeter() {
		return this.jarakRumahSekolahMeter;
	}

	public void setJarakRumahSekolahMeter(float jarakRumahSekolahMeter) {
		this.jarakRumahSekolahMeter = jarakRumahSekolahMeter;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public int getJumlahSaudaraKandung() {
		return this.jumlahSaudaraKandung;
	}

	public void setJumlahSaudaraKandung(int jumlahSaudaraKandung) {
		this.jumlahSaudaraKandung = jumlahSaudaraKandung;
	}

	public String getKegemaran() {
		return this.kegemaran;
	}

	public void setKegemaran(String kegemaran) {
		this.kegemaran = kegemaran;
	}

	public String getKeteranganStatus() {
		return this.keteranganStatus;
	}

	public void setKeteranganStatus(String keteranganStatus) {
		this.keteranganStatus = keteranganStatus;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNamaAyahKandung() {
		return this.namaAyahKandung;
	}

	public void setNamaAyahKandung(String namaAyahKandung) {
		this.namaAyahKandung = namaAyahKandung;
	}

	public String getNamaIbuKandung() {
		return this.namaIbuKandung;
	}

	public void setNamaIbuKandung(String namaIbuKandung) {
		this.namaIbuKandung = namaIbuKandung;
	}

	public String getNamaLengkap() {
		return this.namaLengkap;
	}

	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}

	public String getNamaPanggilan() {
		return this.namaPanggilan;
	}

	public void setNamaPanggilan(String namaPanggilan) {
		this.namaPanggilan = namaPanggilan;
	}

	public String getNamaWali() {
		return this.namaWali;
	}

	public void setNamaWali(String namaWali) {
		this.namaWali = namaWali;
	}

	public String getNik() {
		return this.nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNisn() {
		return this.nisn;
	}

	public void setNisn(String nisn) {
		this.nisn = nisn;
	}

	public String getNoHp() {
		return this.noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getPekerjaanAyah() {
		return this.pekerjaanAyah;
	}

	public void setPekerjaanAyah(String pekerjaanAyah) {
		this.pekerjaanAyah = pekerjaanAyah;
	}

	public String getPekerjaanIbu() {
		return this.pekerjaanIbu;
	}

	public void setPekerjaanIbu(String pekerjaanIbu) {
		this.pekerjaanIbu = pekerjaanIbu;
	}

	public String getPekerjaanWali() {
		return this.pekerjaanWali;
	}

	public void setPekerjaanWali(String pekerjaanWali) {
		this.pekerjaanWali = pekerjaanWali;
	}

	public String getPendidikanAyah() {
		return this.pendidikanAyah;
	}

	public void setPendidikanAyah(String pendidikanAyah) {
		this.pendidikanAyah = pendidikanAyah;
	}

	public String getPendidikanIbu() {
		return this.pendidikanIbu;
	}

	public void setPendidikanIbu(String pendidikanIbu) {
		this.pendidikanIbu = pendidikanIbu;
	}

	public String getPendidikanWali() {
		return this.pendidikanWali;
	}

	public void setPendidikanWali(String pendidikanWali) {
		this.pendidikanWali = pendidikanWali;
	}

	public int getPenghasilanAyahBulanan() {
		return this.penghasilanAyahBulanan;
	}

	public void setPenghasilanAyahBulanan(int penghasilanAyahBulanan) {
		this.penghasilanAyahBulanan = penghasilanAyahBulanan;
	}

	public int getPenghasilanIbuBulanan() {
		return this.penghasilanIbuBulanan;
	}

	public void setPenghasilanIbuBulanan(int penghasilanIbuBulanan) {
		this.penghasilanIbuBulanan = penghasilanIbuBulanan;
	}

	public int getPenghasilanWaliBulanan() {
		return this.penghasilanWaliBulanan;
	}

	public void setPenghasilanWaliBulanan(int penghasilanWaliBulanan) {
		this.penghasilanWaliBulanan = penghasilanWaliBulanan;
	}

	public int getPrestasiId() {
		return this.prestasiId;
	}

	public void setPrestasiId(int prestasiId) {
		this.prestasiId = prestasiId;
	}

	public String getStatusProfil() {
		return this.statusProfil;
	}

	public void setStatusProfil(String statusProfil) {
		this.statusProfil = statusProfil;
	}

	public Date getTanggalLahir() {
		return this.tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public Date getTanggalLahirAyah() {
		return this.tanggalLahirAyah;
	}

	public void setTanggalLahirAyah(Date tanggalLahirAyah) {
		this.tanggalLahirAyah = tanggalLahirAyah;
	}

	public Date getTanggalLahirIbu() {
		return this.tanggalLahirIbu;
	}

	public void setTanggalLahirIbu(Date tanggalLahirIbu) {
		this.tanggalLahirIbu = tanggalLahirIbu;
	}

	public Date getTanggalLahirWali() {
		return this.tanggalLahirWali;
	}

	public void setTanggalLahirWali(Date tanggalLahirWali) {
		this.tanggalLahirWali = tanggalLahirWali;
	}

	public String getTempatLahir() {
		return this.tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public String getTempatLahirAyah() {
		return this.tempatLahirAyah;
	}

	public void setTempatLahirAyah(String tempatLahirAyah) {
		this.tempatLahirAyah = tempatLahirAyah;
	}

	public String getTempatLahirIbu() {
		return this.tempatLahirIbu;
	}

	public void setTempatLahirIbu(String tempatLahirIbu) {
		this.tempatLahirIbu = tempatLahirIbu;
	}

	public String getTempatLahirWali() {
		return this.tempatLahirWali;
	}

	public void setTempatLahirWali(String tempatLahirWali) {
		this.tempatLahirWali = tempatLahirWali;
	}

	public String getTinggalBersama() {
		return this.tinggalBersama;
	}

	public void setTinggalBersama(String tinggalBersama) {
		this.tinggalBersama = tinggalBersama;
	}

	public float getTinggiBadanCm() {
		return this.tinggiBadanCm;
	}

	public void setTinggiBadanCm(float tinggiBadanCm) {
		this.tinggiBadanCm = tinggiBadanCm;
	}

	public Sekolah getSekolah() {
		return this.sekolah;
	}

	public void setSekolah(Sekolah sekolah) {
		this.sekolah = sekolah;
	}

}