package com.toriatec.siakad.admin.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the sekolah database table.
 * 
 */
@Entity
@NamedQuery(name="Sekolah.findAll", query="SELECT s FROM Sekolah s")
public class Sekolah implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="alamat_sekolah")
	private String alamatSekolah;

	@Column(name="created_by")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="id_kabupaten_sekolah")
	private int idKabupatenSekolah;

	@Column(name="id_kecamatan_sekolah")
	private int idKecamatanSekolah;

	@Column(name="id_kelurahan_sekolah")
	private int idKelurahanSekolah;

	@Column(name="id_provinsi_sekolah")
	private int idProvinsiSekolah;

	@Column(name="modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="nama_sekolah")
	private String namaSekolah;

	@Column(name="no_telp")
	private String noTelp;

	private String status;

	//bi-directional many-to-one association to Siswa
	@OneToMany(mappedBy="sekolah")
	private List<Siswa> siswas;

	public Sekolah() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAlamatSekolah() {
		return this.alamatSekolah;
	}

	public void setAlamatSekolah(String alamatSekolah) {
		this.alamatSekolah = alamatSekolah;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getIdKabupatenSekolah() {
		return this.idKabupatenSekolah;
	}

	public void setIdKabupatenSekolah(int idKabupatenSekolah) {
		this.idKabupatenSekolah = idKabupatenSekolah;
	}

	public int getIdKecamatanSekolah() {
		return this.idKecamatanSekolah;
	}

	public void setIdKecamatanSekolah(int idKecamatanSekolah) {
		this.idKecamatanSekolah = idKecamatanSekolah;
	}

	public int getIdKelurahanSekolah() {
		return this.idKelurahanSekolah;
	}

	public void setIdKelurahanSekolah(int idKelurahanSekolah) {
		this.idKelurahanSekolah = idKelurahanSekolah;
	}

	public int getIdProvinsiSekolah() {
		return this.idProvinsiSekolah;
	}

	public void setIdProvinsiSekolah(int idProvinsiSekolah) {
		this.idProvinsiSekolah = idProvinsiSekolah;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNamaSekolah() {
		return this.namaSekolah;
	}

	public void setNamaSekolah(String namaSekolah) {
		this.namaSekolah = namaSekolah;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Siswa> getSiswas() {
		return this.siswas;
	}

	public void setSiswas(List<Siswa> siswas) {
		this.siswas = siswas;
	}

	public Siswa addSiswa(Siswa siswa) {
		getSiswas().add(siswa);
		siswa.setSekolah(this);

		return siswa;
	}

	public Siswa removeSiswa(Siswa siswa) {
		getSiswas().remove(siswa);
		siswa.setSekolah(null);

		return siswa;
	}

}