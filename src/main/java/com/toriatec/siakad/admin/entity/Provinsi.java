package com.toriatec.siakad.admin.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the provinsi database table.
 * 
 */
@Entity
@NamedQuery(name="Provinsi.findAll", query="SELECT p FROM Provinsi p")
public class Provinsi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String name;

	//bi-directional many-to-one association to Kabupaten
	@OneToMany(mappedBy="provinsi")
	private List<Kabupaten> kabupatens;

	public Provinsi() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Kabupaten> getKabupatens() {
		return this.kabupatens;
	}

	public void setKabupatens(List<Kabupaten> kabupatens) {
		this.kabupatens = kabupatens;
	}

	public Kabupaten addKabupaten(Kabupaten kabupaten) {
		getKabupatens().add(kabupaten);
		kabupaten.setProvinsi(this);

		return kabupaten;
	}

	public Kabupaten removeKabupaten(Kabupaten kabupaten) {
		getKabupatens().remove(kabupaten);
		kabupaten.setProvinsi(null);

		return kabupaten;
	}

}