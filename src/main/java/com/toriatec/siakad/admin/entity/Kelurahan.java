package com.toriatec.siakad.admin.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the kelurahan database table.
 * 
 */
@Entity
@NamedQuery(name="Kelurahan.findAll", query="SELECT k FROM Kelurahan k")
public class Kelurahan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String name;

	//bi-directional many-to-one association to Kecamatan
	@ManyToOne
	@JoinColumn(name="id_kecamatan")
	private Kecamatan kecamatan;

	public Kelurahan() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Kecamatan getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(Kecamatan kecamatan) {
		this.kecamatan = kecamatan;
	}

}