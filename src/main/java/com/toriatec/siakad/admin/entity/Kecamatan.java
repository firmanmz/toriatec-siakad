package com.toriatec.siakad.admin.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the kecamatan database table.
 * 
 */
@Entity
@NamedQuery(name="Kecamatan.findAll", query="SELECT k FROM Kecamatan k")
public class Kecamatan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String name;

	//bi-directional many-to-one association to Kabupaten
	@ManyToOne
	@JoinColumn(name="id_kabupaten")
	private Kabupaten kabupaten;

	//bi-directional many-to-one association to Kelurahan
	@OneToMany(mappedBy="kecamatan")
	private List<Kelurahan> kelurahans;

	public Kecamatan() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Kabupaten getKabupaten() {
		return this.kabupaten;
	}

	public void setKabupaten(Kabupaten kabupaten) {
		this.kabupaten = kabupaten;
	}

	public List<Kelurahan> getKelurahans() {
		return this.kelurahans;
	}

	public void setKelurahans(List<Kelurahan> kelurahans) {
		this.kelurahans = kelurahans;
	}

	public Kelurahan addKelurahan(Kelurahan kelurahan) {
		getKelurahans().add(kelurahan);
		kelurahan.setKecamatan(this);

		return kelurahan;
	}

	public Kelurahan removeKelurahan(Kelurahan kelurahan) {
		getKelurahans().remove(kelurahan);
		kelurahan.setKecamatan(null);

		return kelurahan;
	}

}