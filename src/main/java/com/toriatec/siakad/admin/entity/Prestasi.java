package com.toriatec.siakad.admin.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the prestasi database table.
 * 
 */
@Entity
@NamedQuery(name="Prestasi.findAll", query="SELECT p FROM Prestasi p")
public class Prestasi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="created_by")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="jenis_prestasi")
	private String jenisPrestasi;

	private String keterangan;

	@Column(name="modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="nama_prestasi")
	private String namaPrestasi;

	private String penyelenggara;

	private int tahun;

	@Column(name="tingkat_prestasi")
	private String tingkatPrestasi;

	public Prestasi() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getJenisPrestasi() {
		return this.jenisPrestasi;
	}

	public void setJenisPrestasi(String jenisPrestasi) {
		this.jenisPrestasi = jenisPrestasi;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNamaPrestasi() {
		return this.namaPrestasi;
	}

	public void setNamaPrestasi(String namaPrestasi) {
		this.namaPrestasi = namaPrestasi;
	}

	public String getPenyelenggara() {
		return this.penyelenggara;
	}

	public void setPenyelenggara(String penyelenggara) {
		this.penyelenggara = penyelenggara;
	}

	public int getTahun() {
		return this.tahun;
	}

	public void setTahun(int tahun) {
		this.tahun = tahun;
	}

	public String getTingkatPrestasi() {
		return this.tingkatPrestasi;
	}

	public void setTingkatPrestasi(String tingkatPrestasi) {
		this.tingkatPrestasi = tingkatPrestasi;
	}

}