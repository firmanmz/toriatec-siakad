package com.toriatec.siakad.admin.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the kabupaten database table.
 * 
 */
@Entity
@NamedQuery(name="Kabupaten.findAll", query="SELECT k FROM Kabupaten k")
public class Kabupaten implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String name;

	//bi-directional many-to-one association to Provinsi
	@ManyToOne
	@JoinColumn(name="id_provinsi")
	private Provinsi provinsi;

	//bi-directional many-to-one association to Kecamatan
	@OneToMany(mappedBy="kabupaten")
	private List<Kecamatan> kecamatans;

	public Kabupaten() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Provinsi getProvinsi() {
		return this.provinsi;
	}

	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}

	public List<Kecamatan> getKecamatans() {
		return this.kecamatans;
	}

	public void setKecamatans(List<Kecamatan> kecamatans) {
		this.kecamatans = kecamatans;
	}

	public Kecamatan addKecamatan(Kecamatan kecamatan) {
		getKecamatans().add(kecamatan);
		kecamatan.setKabupaten(this);

		return kecamatan;
	}

	public Kecamatan removeKecamatan(Kecamatan kecamatan) {
		getKecamatans().remove(kecamatan);
		kecamatan.setKabupaten(null);

		return kecamatan;
	}

}