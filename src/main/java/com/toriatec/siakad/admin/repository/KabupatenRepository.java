package com.toriatec.siakad.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.toriatec.siakad.admin.entity.Kabupaten;

public interface KabupatenRepository extends JpaRepository<Kabupaten, Integer>{

}
