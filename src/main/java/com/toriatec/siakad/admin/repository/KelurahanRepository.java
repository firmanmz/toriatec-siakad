package com.toriatec.siakad.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.toriatec.siakad.admin.entity.Kelurahan;

public interface KelurahanRepository extends JpaRepository<Kelurahan, Integer>{

}
