package com.toriatec.siakad.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.toriatec.siakad.admin.entity.Sekolah;

public interface SekolahRepository extends JpaRepository<Sekolah, Integer>{
	
	
	
}
