package com.toriatec.siakad.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.toriatec.siakad.admin.entity.Provinsi;

public interface ProvinsiRepository extends JpaRepository<Provinsi, Integer>{

}
