package com.toriatec.siakad.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.toriatec.siakad.admin.entity.Siswa;


@Repository
public interface SiswaRepository extends JpaRepository<Siswa, Integer>{

	@Query("SELECT s FROM Siswa s WHERE s.nik = :nik") 
    public Siswa findByNik(@Param("nik") String nik);
	
}
