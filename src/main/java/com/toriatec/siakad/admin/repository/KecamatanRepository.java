package com.toriatec.siakad.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.toriatec.siakad.admin.entity.Kecamatan;

public interface KecamatanRepository extends JpaRepository<Kecamatan, Integer>{

}
