package com.toriatec.siakad.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.toriatec.siakad.admin.entity.Siswa;
import com.toriatec.siakad.admin.service.SiswaService;

@RestController
@RequestMapping(value = "api/siswa")
public class SiswaRestController {

	@Autowired
	SiswaService siswaService;

	@GetMapping(value = "/all")
	public List<Siswa> getSiswa() {
		return siswaService.findAll();
	}

	@GetMapping(value = "/nik/{nik}")
	public Siswa getSiswaByNik(@PathVariable("nik") String nik) {
		return siswaService.getByNik(nik);
	}
	
	@PostMapping(value = "/save")
	public void saveOrUpdate(@RequestBody Siswa siswa) {
		siswaService.saveOrUpdate(siswa);
	}
	
}
