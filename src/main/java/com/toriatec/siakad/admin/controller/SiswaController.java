package com.toriatec.siakad.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("admin/siswa")
public class SiswaController {

	@RequestMapping(value = "")
	public ModelAndView dashboard(Model model) {
		ModelAndView view = null;
		view = new ModelAndView("views/admin/siswa");
		model.addAttribute("module", "siswa");
		return view;
	}
	
}
