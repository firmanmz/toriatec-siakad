package com.toriatec.siakad.admin.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toriatec.siakad.admin.entity.Siswa;
import com.toriatec.siakad.admin.repository.SiswaRepository;


@Service
public class SiswaService {

	@Autowired
	SiswaRepository siswaRepository;
	
	public List<Siswa> findAll(){
		return siswaRepository.findAll();
	}
	
	public Siswa getByNik(String nik) {
		return siswaRepository.findByNik(nik);
	}
	
	public Siswa findOne(int id) {
		return siswaRepository.findOne(id);
	}
	
	public void saveOrUpdate(Siswa siswa) {
		if(siswa.getId() > 0) {
			//update siswa
			siswa.setModifiedBy("SYS");
			siswa.setModifiedDate(new Date());
			//data yg gak boleh diubah
			Siswa siswaTemp = findOne(siswa.getId());
			siswa.setCreatedBy(siswaTemp.getCreatedBy());
			siswa.setCreatedDate(siswaTemp.getCreatedDate());
		}else {
			//save siswa
			siswa.setCreatedBy("SYS");
			siswa.setCreatedDate(new Date());
		}
		siswaRepository.save(siswa);
	}
	
}
